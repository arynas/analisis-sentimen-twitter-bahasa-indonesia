Analisis Sentimen Twitter Bahasa Indonesia menggunakan SentiStrength.

Referensi:

- https://github.com/masdevid/sentistrength_id
- Wahid, D. H., & Azhari, S. N. (2016). Peringkasan Sentimen Esktraktif di Twitter Menggunakan Hybrid TF-IDF dan Cosine Similarity. IJCCS (Indonesian Journal of Computing and Cybernetics Systems), 10(2), 207-218.
